//try - catch используют чтоб получать оповещения
// об ошибках допущеных не только в синтаксисе кода,
// а и во входных данных, таких как распаршеный json и массивы


const books = [
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];



const root = document.querySelector(".root")
const filteredBooks = []
function create(array) {
  array.forEach(element => {
      try {
        if (!('author' in element)) {
            throw new SyntaxError('Current book has no author property')
        } else if (!('name' in element)) {
            throw new SyntaxError('Current book has no name property')
        } else if (!('price' in element)) {
            throw new SyntaxError('Current book has no price property')
        } else {
          filteredBooks.push(element);
        }
    } catch(e) {
      console.log('Ошибка ' + "в книге с названием " + "   " + element.name +  "   " + e.name + ":" + e.message + "\n" + e.stack)
    }
  })
}
create(books)
filteredBooks.forEach(elem => {
const bookName = document.createElement("ul")
bookName.innerHTML = `книга "${elem.name}"`
root.appendChild(bookName)
for (let value in elem) {
  const book = document.createElement("li")
  if(value == "name") {
    book.innerHTML = `название: ${elem[value]}`
  }
  if(value == "author") {
    book.innerHTML = `автор: ${elem[value]}`
  }
  if(value == "price") {
    book.innerHTML = `цена: ${elem[value]}$`
  }
  bookName.appendChild(book)
}
})
